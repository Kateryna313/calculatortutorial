﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Globalization;

public class Calculator : MonoBehaviour
{
    //Main operation output
    [SerializeField]
    Text inputField;

    //Output of multiple operations
    [SerializeField]
    Text inputFieldFullOperation;

    //Two number for mathematical operations
    double[] number = new double[2];
    int i = 0;

    //Change operation symbol
    string operatorSymbol = "";

    //For output bundles
    string inputValue;
    double result = 0;

    //cut full input view to 
    int cutFullInputView = 25;

    //Use this when make long number
    string num = "";

    //for block button
    string symbolAddSymb = " ";
    string symbolDeleteSymb = "*";

    //bool
    bool enterQuery = false;
    
    //for change symbol and beautiful conclusion (don't repit)
    int changeOperatorSymbol = 0;

    //for make one enter =, when have two (==) and operation when pressed again on equal
    double secondNumber = 0.0;
    double firstNumber = 0.0;

    //For have possible a use point when converting
    IFormatProvider formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };

    //First func. Fires when button are pressed. ButtonPressed()
    #region
    public void ButtonPressed()
    { 
        //if try delete by 0, not possible make operation after. Clear for good work 
        if(result == Mathf.Infinity || inputField.text == "NaN")
        {
            ClearResult();
        }

        //put the text of the pressed button in the variable
        string buttonValue = EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text;

        DisplayingOperations(buttonValue);
        WhichButtonPressed(buttonValue);    
    }
    #endregion

    //Displaying operations on the screen. DisplayingOperations(string buttonValue)
    #region
    public void DisplayingOperations(string buttonValue)
    {
        //Check for one point in view number and check possible button press
        if (buttonValue != "." && !buttonValue.Contains(symbolAddSymb))
        {
            //sign rewriting. erase the previous one and then whrite a new one
            if ((buttonValue == "x" || buttonValue == "÷" || buttonValue == "%" || buttonValue == "-" || buttonValue == "+") && i >= 1 && changeOperatorSymbol != 0)
            {
                inputFieldFullOperation.text = inputFieldFullOperation.text.Substring(0, inputFieldFullOperation.text.Length - 1);
                inputValue = inputValue.Remove(inputValue.Length - 1);
                Debug.Log("Test 80");
            }

            //Display the entered character on the screen
            //Check if it can be written
            if(!inputField.text.Contains("E"))
            {
                inputValue += buttonValue;
            }
            inputFieldFullOperation.text += buttonValue;
        }
    }
    #endregion

    //Which button is pressed in calc and make number. WhichButtonPressed(string buttonValue)
    #region
    public void WhichButtonPressed(string buttonValue)
    { 
        //Out button value
        int arg;
        //Check button number, point or symbol operation
        if (int.TryParse(buttonValue, out arg) || buttonValue == "." || buttonValue == "00")
        { 
            //If don't first operation make firs operand
            if (i > 1)
            {
                i = 1;
                if (result != 0)
                {
                    number[0] = result;
                }
                else
                {
                    number[0] = firstNumber;
                }
            }

            //Check enter query or not. If checked change argument
            if (enterQuery == true)
            {
                number = new double[2];
                i = 0;
                num = "";
                enterQuery = false;
                inputField.text = "";
                inputFieldFullOperation.text = buttonValue;

            }

            changeOperatorSymbol++;

            //For get the correct fraction
            if (buttonValue == "." && !num.Contains(".") && !inputValue.Contains(".") || buttonValue == "00")
            {
                if (buttonValue == "." && num.Length > 0)
                {
                    num += ".";
                    inputValue += buttonValue;
                    inputFieldFullOperation.text += buttonValue;
                }
                else if (buttonValue == "." && num.Length == 0)
                {
                    if (!inputField.text.Contains("Enter"))
                    {
                        num += "0.";
                    }
                    else
                    {
                        num += ".";
                    }
                    inputValue += num;
                    inputFieldFullOperation.text += num;
                }
                else
                {
                    num += "00";
                }
            }
            else if (buttonValue != ".")
            {
                num += arg;
            }
            
            //Making the number of levels
            number[i] = Convert.ToDouble(num, formatter);

            if (i == 1)
            {
                secondNumber = number[i];
                if (num.Length == 1)
                {
                    ActiveOrNotSymbol(symbolAddSymb);
                }
            }
            else
            {
                firstNumber = number[i];
                inputValue = number[i].ToString();
                if(num.Length == 1)
                {
                    inputFieldFullOperation.text = num;
                }
            }
        }
        else
        {
            i++;
            enterQuery = false;
            changeOperatorSymbol ++;
            //We assign a symbol to operator
            switch (buttonValue)
            {
                case "+":
                    operatorSymbol = buttonValue;
                    num = "";
                    break;
                case "-":
                    if (number[0] == 0 && result == 0)
                    {
                        num = "-";
                    }
                    operatorSymbol = buttonValue;
                    num = "";
                    break;
                case "÷":
                    operatorSymbol = buttonValue;
                    num = "";
                    break;
                case "x":
                    operatorSymbol = buttonValue;
                    num = "";
                    break;
                case "%":
                    operatorSymbol = buttonValue;
                    num = "";
                    break;
                case "=":
                    enterQuery = true;
                    
                    ActiveOrNotSymbol(symbolDeleteSymb);
                    OperetionSymbols(operatorSymbol);
                    inputFieldFullOperation.text += inputValue;
                    if (changeOperatorSymbol == 1)
                    {
                        firstNumber = Convert.ToDouble(inputField.text);
                        number[0] = EnterTwoTimesEquells(firstNumber, secondNumber);
                        //firstNumber = number[0];
                        number[1] = secondNumber;
                        double temp = number[0];
                        inputValue = temp.ToString();
                        inputFieldFullOperation.text = " " + firstNumber.ToString() + operatorSymbol + secondNumber.ToString() + "=" + temp;

                    }
                    changeOperatorSymbol = 0;
                   
                    //If line big, cut it
                    if (inputFieldFullOperation.text.Length > cutFullInputView)
                    {
                        inputFieldFullOperation.text = result.ToString();
                    }
                    break;
            }
          
        }
        inputField.text = inputValue;
    }
    #endregion

    //If enter = two times. Make operation what did before. EnterTwoTimesEquells(double num1, double num2)
    #region
    public double EnterTwoTimesEquells(double num1, double num2)
    {
        switch (operatorSymbol)
        {
            case "+":
                result = num1 + num2;
                break;
            case "-":
                result = num1 - num2;
                break;
            case "÷":
                if (num2 == 0 || num2 == 00)
                {
                    result = Mathf.Infinity;
                    break;
                }
                result = num1 / num2;
                break;
            case "x":
                result = num1 * num2;
                break;
            case "%":
                result = num1 % num2;
                break;
        }
        return (Math.Round(result, 2));
    }
    #endregion

    //Operation Symbol. OperetionSymbols(string operatorSymbol)
    #region
    public void OperetionSymbols(string operatorSymbol)
    {
        switch (operatorSymbol)
        {
            case "+":
                result = Math.Round(number[0] + number[1], 2);
                break;
            case "-":
                result = Math.Round(number[0] - number[1], 2);
                break;
            case "÷":
                if (number[1] == 0 || number[1] == 00)
                {
                    result = Mathf.Infinity;
                    break;
                }
                result = Math.Round(number[0] / number[1], 2);
                break;
            case "x":
                result = Math.Round(number[0] * number[1], 2);
                break;
            case "%":
                result = Math.Round(number[0] % number[1], 2);
                break;
        }

        number = new double[2];
        inputValue = result.ToString();
    }
    #endregion

    //Change minus or plus in the number. ChangeMinusPlus()
    #region
    public void ChangeMinusPlus()
    {
        if (!inputField.text.Contains("Enter"))
        {
            if (number[0] != 0 && result == 0)
            {
                number[0] *= -1;
                inputFieldFullOperation.text = number[0].ToString();
                inputField.text = number[0].ToString();
                inputValue = number[0].ToString();
                num = number[0].ToString();
                firstNumber = number[0];
            }
            else if (result != 0)
            {
                result *= -1;
                inputValue = result.ToString();
                inputField.text = result.ToString();
                inputFieldFullOperation.text = result.ToString();
                number[0] = result;
            }
        }
    }
    #endregion

    //Make the number square. BtnNumSquare()
    #region
    public void BtnNumSquare()
    {
        if (!inputField.text.Contains("Enter"))
        {
            
            if (result == 0)
            {
                inputFieldFullOperation.text = number[0].ToString() + "*" + number[0].ToString();
                result = Math.Round(number[0] * number[0], 2);
                number[0] = result;
                
            }
            else
            {
                inputFieldFullOperation.text = result.ToString() + "*" + result.ToString();
                result = Math.Round(result * result, 2);
            }
            inputValue = result.ToString();
            inputField.text = inputValue;
            inputFieldFullOperation.text += "=" + inputValue;
        }
    }
    #endregion

    //Find the root of the number. BtnRoot()
    #region
    public void BtnRoot()
    {
        if (!inputField.text.Contains("Enter"))
        {
            if (result == 0)
            {
                inputFieldFullOperation.text = number[0].ToString() + "/" + number[0].ToString();
                result = Math.Round(Math.Sqrt(Convert.ToDouble(number[0])), 2);
                number[0] = result;
            }
            else
            {
                inputFieldFullOperation.text = result.ToString() + "/" + result.ToString();
                result = Math.Round(Math.Sqrt(Convert.ToDouble(inputValue)), 2);
            }
            inputValue = result.ToString();
            inputField.text = inputValue;
            inputFieldFullOperation.text += "=" + inputValue;
        }
    }
    #endregion

    //Clear result. For MC use this function too. ClearResult()
    #region
    public void ClearResult()
    {
        i = 0;
        num = "";
        result = 0.0;
        inputValue = "";
        firstNumber = 0;
        secondNumber = 0;
        operatorSymbol = "";
        inputField.text = "";
        number = new double[2];
        changeOperatorSymbol = 0;
        inputFieldFullOperation.text = "";
        ActiveOrNotSymbol(symbolDeleteSymb);
    }
    #endregion

    //Make activ or not active symbol. ActiveOrNotSymbol(string symb)
    #region
    public void ActiveOrNotSymbol(string symb)
    {
        GameObject bBtnPlus = GameObject.Find("btnPlus");
        GameObject bBtnPlusMinus = GameObject.Find("btnPlusMinus");
        GameObject bBtnNumSquared = GameObject.Find("btnNumSquared");
        GameObject bBtnRoot = GameObject.Find("btnRoot");
        GameObject bBtnDivision = GameObject.Find("btnDivision");
        GameObject bBtnRemainderDivision = GameObject.Find("btnRemainderDivision");
        GameObject bBtnMinus = GameObject.Find("btnMinus");
        GameObject bBtnMultiplication = GameObject.Find("btnMultiplication");

        if (symb == symbolAddSymb)
        {
            bBtnPlus.GetComponentInChildren<Text>().text += symb;
            bBtnPlusMinus.GetComponentInChildren<Text>().text = "+/" + symb;
            bBtnNumSquared.GetComponentInChildren<Text>().text += symb;
            bBtnRoot.GetComponentInChildren<Text>().text += symb;
            bBtnDivision.GetComponentInChildren<Text>().text += symb;
            bBtnRemainderDivision.GetComponentInChildren<Text>().text += symb;
            bBtnMinus.GetComponentInChildren<Text>().text += symb;
            bBtnMultiplication.GetComponentInChildren<Text>().text += symb;
            inputValue = "Enter equals or more number";
        }
        else
        {
            bBtnPlus.GetComponentInChildren<Text>().text = "+";
            bBtnPlusMinus.GetComponentInChildren<Text>().text = "+/-";
            bBtnNumSquared.GetComponentInChildren<Text>().text = "x²";
            bBtnRoot.GetComponentInChildren<Text>().text = "√";
            bBtnDivision.GetComponentInChildren<Text>().text = "÷";
            bBtnRemainderDivision.GetComponentInChildren<Text>().text = "%";
            bBtnMinus.GetComponentInChildren<Text>().text = "-";
            bBtnMultiplication.GetComponentInChildren<Text>().text = "x";
            inputField.text = result.ToString();
            
        }
    }
    #endregion
}





